# Herobrine for Bukkit or Spigot #

----

[Herobrine Home Page](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=1) | [Plugin Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=2) | [Changelog](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=5) | [Configuration](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=4) | [Commands & Permissions](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=3) | [Internal Bug Reporting](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=24)

----

**Follow this project on Twitter [@DavidBerdik](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=6). Announcements of new releases will be posted with the [#HerobrinePlugin](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=7) hashtag. To track the plugin's development, follow [@TPWHerobrine](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=26).**

----

**Current Release Version: v1.8.0 (compiled from commit 0262059 for Spigot 1.13.2)**

**[Download latest version now.](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=9) A complete listing of all versions of Herobrine is available under the "[Plugin Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=10)" tab.**

Herobrine for Bukkit or Spigot (or just "Herobrine" for short) is a derivative of the [Herobrine AI](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=11) plugin for Bukkit, originally written by Bukkit plugin developer [Jakub1221](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=12). This derivative is intended to maintain a rapid release cycle in an effort to patch bugs more frequently and also to try to update to the newest release of Bukkit/Spigot faster so that server administrators will be able to continue to have Herobrine stalk the players on their Minecraft server.
 
### What software license is this plugin distributed under? ###

As per the requirements set by the original developer, Herobrine for Bukkit or Spigot is distributed under the MIT License. The original copy of the MIT License that was distributed with Herobrine AI can be found [here](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=13).

### How do I contribute? ###

To contribute to the project, fork the Herobrine for Bukkit or Spigot repository, make whatever changes you desire to the code, and submit a pull request. I will review your changes and merge them if they are acceptable. Any changes, whether they are bug fixes or new features, are welcome.

In order to contribute, you will need to acquire Bukkit by making use of the [Spigot BuildTools](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=14). After you have compiled Bukkit, place a copy of the compiled build of Bukkit jar in the folder named 'libs' within your copy of the Eclipse workspace.

### Contribution guidelines ###

Pull requests that do not provide detail on what changes were made to the code will be denied without a review. Please provide adequate information on the changes you made to the code. **If you are planning to submit a pull request for your changes, please use [Eclipse](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=15) as your Java IDE to ensure that the file structure remains the same. If you use a different IDE, the file structure will be altered and I will not be able to easily merge your changes.**