package net.theprogrammersworld.herobrine.AI.extensions;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.misc.StructureLoader;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

public class GraveyardWorld {

	public static void create() {
		final Location loc = new Location(Bukkit.getServer().getWorld(Herobrine.getPluginCore().getConfigDB().HerobrineWorldName), 0.0, 3.0, 0.0);
		for (int x = -50; x <= 50; ++x) {
			for (int z = -50; z <= 50; ++z) {
				loc.getWorld().getBlockAt(x, 3, z).setType(Material.MYCELIUM);
			}
		}
		final int MainX = -10;
		final int MainY = 3;
		final int MainZ = -10;
		new StructureLoader(Herobrine.getPluginCore().getInputStreamData("/res/graveyard_world.yml")).Build(loc.getWorld(), MainX, MainY, MainZ);
	}

}