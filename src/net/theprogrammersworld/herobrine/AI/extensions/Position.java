package net.theprogrammersworld.herobrine.AI.extensions;

import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;

import org.bukkit.Location;

public class Position {

	public static Location getTeleportPosition(final Location ploc) {
		final int chance = new Random().nextInt(3);
		if (chance == 0) {
			if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			}
		} else if (chance == 1) {
			if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			}
		} else if (chance == 2) {
			if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			}
		}
		if (chance == 3) {
			if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() + 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() + 2).getType())) {
				ploc.setZ(ploc.getZ() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() - 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() - 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY(), ploc.getBlockZ()).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX() + 2, ploc.getBlockY() + 1, ploc.getBlockZ()).getType())) {
				ploc.setX(ploc.getX() + 2.0);
			} else if (Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY(), ploc.getBlockZ() - 2).getType())
					&& Herobrine.isAllowedBlock(ploc.getWorld().getBlockAt(ploc.getBlockX(), ploc.getBlockY() + 1, ploc.getBlockZ() - 2).getType())) {
				ploc.setZ(ploc.getZ() - 2.0);
			}
		}
		return ploc;
	}

}