package net.theprogrammersworld.herobrine.AI;

import net.theprogrammersworld.herobrine.Herobrine;

public abstract class Core {

	private final AppearType appear;
	private final CoreType coreType;

	private CoreResult nowData;

	public Core(final CoreType cp, final AppearType ap) {
		coreType = cp;
		appear = ap;
	}

	public AppearType getAppear() {
		return appear;
	}

	public CoreType getCoreType() {
		return coreType;
	}

	protected abstract CoreResult callCore(final Object[] p0);

	public CoreResult runCore(final Object[] data) {
		nowData = callCore(data);
		if (nowData.getResult() && (appear == AppearType.APPEAR)) {
			Herobrine.getPluginCore().getAICore().setCoreTypeNow(coreType);
		}
		return nowData;
	}

	public enum AppearType {
		APPEAR, NORMAL
	}

	public enum CoreType {
		ATTACK,  HAUNT,  BOOK,  BUILD_STUFF,  BURY_PLAYER,
		DESTROY_TORCHES,  GRAVEYARD,  PYRAMID,  RANDOM_POSITION,
		SIGNS,  SOUNDF,  TOTEM,  ANY,  START,  TEMPLE,  HEADS,
		RANDOM_SOUND,  RANDOM_EXPLOSION,  BURN,  CURSE,  STARE;
	}

}