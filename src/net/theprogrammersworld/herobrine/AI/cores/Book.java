package net.theprogrammersworld.herobrine.AI.cores;

import java.util.ArrayList;
import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class Book extends Core {

	public Book() {
		super(CoreType.BOOK, AppearType.NORMAL);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		final Player player = (Player) data[0];
		if (Herobrine.getPluginCore().getConfigDB().useWorlds.contains(player.getLocation().getWorld().getName())) {
			if (!Herobrine.getPluginCore().getConfigDB().WriteBooks || !Herobrine.getPluginCore().getSupport().checkBooks(player.getLocation())) {
				return new CoreResult(false, player.getDisplayName() + " is in a world that Herobrine cannot haunt.");
			}
			final int chance = new Random().nextInt(100);
			if (chance <= (100 - Herobrine.getPluginCore().getConfigDB().BookChance)) {
				return new CoreResult(false, "Herobrine books are prohibited.");
			}
			final Inventory chest = (Inventory) data[1];
			if (chest.firstEmpty() == -1) {
				return new CoreResult(false, "Herobrine failed to create a book.");
			}
			if (Herobrine.getPluginCore().getAICore().getResetLimits().isBook()) {
				chest.setItem(chest.firstEmpty(), newBook());
				return new CoreResult(true, "Herobrine has created a book.");
			}
		}
		return new CoreResult(false, "Herobrine failed to create a book.");
	}

	public ItemStack newBook() {
		final int count = Herobrine.getPluginCore().getConfigDB().useBookMessages.size();
		final int chance = new Random().nextInt(count);
		final ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		final BookMeta meta = (BookMeta) book.getItemMeta();
		final ArrayList<String> list = new ArrayList<String>();
		meta.setTitle("");
		meta.setAuthor("");
		list.add(0, Herobrine.getPluginCore().getConfigDB().useBookMessages.get(chance));
		meta.setPages(list);
		book.setItemMeta(meta);
		return book;
	}

}