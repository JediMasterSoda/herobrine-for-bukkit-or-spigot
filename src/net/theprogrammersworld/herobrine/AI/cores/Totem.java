package net.theprogrammersworld.herobrine.AI.cores;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;
import net.theprogrammersworld.herobrine.AI.Message;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Totem extends Core {

	public Totem() {
		super(CoreType.TOTEM, AppearType.APPEAR);
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		return TotemCall((Location) data[0], (String) data[1]);
	}

	public CoreResult TotemCall(final Location loc, final String caller) {
		AICore.isTotemCalled = false;
		loc.getWorld().strikeLightning(loc);
		if (Herobrine.getPluginCore().getConfigDB().TotemExplodes) {
			loc.getWorld().createExplosion(loc, 5.0f);
		}
		if (Bukkit.getServer().getPlayer(caller) != null) {
			if (Bukkit.getServer().getPlayer(caller).isOnline()) {
				Herobrine.getPluginCore().getAICore().setCoreTypeNow(CoreType.TOTEM);
				Herobrine.getPluginCore().getAICore().setAttackTarget(Bukkit.getServer().getPlayer(caller));
				final Player player = Bukkit.getServer().getPlayer(caller);
				for (Player oplayer : Bukkit.getOnlinePlayers()) {
					Location ploc = oplayer.getLocation();
					if (
						(oplayer.getName() != player.getName()) &&
						((ploc.getX() + 10.0) > loc.getX()) &&
						((ploc.getX() - 10.0) < loc.getX()) &&
						((ploc.getZ() + 10.0) > loc.getZ()) &&
						((ploc.getZ() - 10.0) < loc.getZ())
					) {
						Message.sendRandomMessage(oplayer);
						if (Herobrine.getPluginCore().getConfigDB().UsePotionEffects) {
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 1));
						}
					}
				}
			} else {
				boolean hasTarget = false;
				Player target = null;
				for (Player oplayer : Bukkit.getOnlinePlayers()) {
					if (!hasTarget) {
						Location ploc = oplayer.getLocation();
						if (((ploc.getX() + 10.0) > loc.getX()) && ((ploc.getX() - 10.0) < loc.getX()) && ((ploc.getZ() + 10.0) > loc.getZ()) && ((ploc.getZ() - 10.0) < loc.getZ())) {
							hasTarget = true;
							target = oplayer;
						}
					}
				}
				if (hasTarget) {
					Herobrine.getPluginCore().getAICore().cancelTarget(CoreType.TOTEM, true);
					Herobrine.getPluginCore().getAICore().setAttackTarget(target);
					final Player player = target;
					for (Player oplayer : Bukkit.getOnlinePlayers()) {
						Location ploc = oplayer.getLocation();
						if ((oplayer.getName() != player.getName()) && ((ploc.getX() + 20.0) > loc.getX()) && ((ploc.getX() - 20.0) < loc.getX()) && ((ploc.getZ() + 20.0) > loc.getZ())
								&& ((ploc.getZ() - 20.0) < loc.getZ())) {
							Message.sendRandomMessage(oplayer);
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 1));
						}
					}
				}
			}
		} else {
			boolean hasTarget = false;
			Player target = null;
			for (Player oplayer : Bukkit.getOnlinePlayers()) {
				if (!hasTarget) {
					Location ploc = oplayer.getLocation();
					if (((ploc.getX() + 20.0) > loc.getX()) && ((ploc.getX() - 20.0) < loc.getX()) && ((ploc.getZ() + 20.0) > loc.getZ()) && ((ploc.getZ() - 20.0) < loc.getZ())) {
						hasTarget = true;
						target = oplayer;
					}
				}
			}
			if (hasTarget) {
				Herobrine.getPluginCore().getAICore().cancelTarget(CoreType.TOTEM, true);
				Herobrine.getPluginCore().getAICore().setAttackTarget(target);
				final Player player = target;
				for (Player oplayer : Bukkit.getOnlinePlayers()) {
					if (oplayer.getEntityId() != Herobrine.herobrineEntityID) {
						Location ploc = oplayer.getLocation();
						if ((oplayer.getName() != player.getName()) && ((ploc.getX() + 20.0) > loc.getX()) && ((ploc.getX() - 20.0) < loc.getX()) && ((ploc.getZ() + 20.0) > loc.getZ())
								&& ((ploc.getZ() - 20.0) < loc.getZ())) {
							Message.sendRandomMessage(oplayer);
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000, 1));
							oplayer.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1000, 1));
						}
					}
				}
			}
		}
		return new CoreResult(false, "Totem called.");
	}

}