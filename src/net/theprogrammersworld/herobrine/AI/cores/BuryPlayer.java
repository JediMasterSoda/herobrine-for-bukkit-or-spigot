package net.theprogrammersworld.herobrine.AI.cores;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.CoreResult;

public class BuryPlayer extends Core {

	public Block savedBlock1;
	public Block savedBlock2;

	public BuryPlayer() {
		super(CoreType.BURY_PLAYER, AppearType.NORMAL);
		savedBlock1 = null;
		savedBlock2 = null;
	}

	@Override
	public CoreResult callCore(final Object[] data) {
		return FindPlace((Player) data[0]);
	}

	public CoreResult FindPlace(final Player player) {
		if (Herobrine.getPluginCore().getSupport().checkBuild(player.getLocation())) {
			final Location loc = player.getLocation();
			if (Herobrine.isSolidBlock(
					loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).getType())
					&& Herobrine.isSolidBlock(
							loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ()).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 1).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ() - 1).getType())
					&& Herobrine.isSolidBlock(
							loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 3, loc.getBlockZ()).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 3, loc.getBlockZ() - 1).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 1).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ() - 1).getType())
					&& Herobrine.isSolidBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 2).getType())
					&& !Herobrine.isAllowedBlock(loc.getWorld()
							.getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() - 2).getType())) {
				if (Herobrine.getPluginCore().getSupport().checkBuild(
						loc.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ()).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ()).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 1).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ() - 1).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 3, loc.getBlockZ()).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 3, loc.getBlockZ() - 1).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 1).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 2, loc.getBlockZ() - 1).getLocation())
						&& Herobrine.getPluginCore().getSupport().checkBuild(loc.getWorld()
								.getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ() - 2).getLocation())) {
					Bury(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), player);
					return new CoreResult(true, player.getDisplayName() + " was buried by Herobrine.");
				} else {
					return new CoreResult(false, "Player in protect zone,You can't bury him.");
				}
			}
		} else {
			return new CoreResult(false, player.getDisplayName()
					+ " could not be buried because a good burial location could not be found.");
		}
		return new CoreResult(false,
				player.getDisplayName() + " could not be buried because a good burial location could not be found.");
	}

	public void Bury(final World world, final int X, final int Y, final int Z, final Player player) {
		final Location loc = new Location(world, X, Y, Z);
		loc.getWorld().getBlockAt(X, Y - 1, Z).setType(Material.AIR);
		loc.getWorld().getBlockAt(X, Y - 2, Z).setType(Material.AIR);
		loc.getWorld().getBlockAt(X, Y - 3, Z).setType(Material.AIR);
		loc.getWorld().getBlockAt(X, Y - 1, Z - 1).setType(Material.AIR);
		loc.getWorld().getBlockAt(X, Y - 2, Z - 1).setType(Material.AIR);
		loc.getWorld().getBlockAt(X, Y - 3, Z - 1).setType(Material.AIR);
		player.teleport(new Location(world, X, Y - 3, Z));
		RegenBlocks(world, X, Y, Z, player.getName());
	}
	
	public void RegenBlocks(final World world, final int X, final int Y, final int Z, final String playername) {
		final Location loc = new Location(world, X, Y, Z);
		final Location signloc = new Location(world, X, Y, Z - 2);
		final Block signblock = signloc.add(0.0, 0.0, 0.0).getBlock();
		signblock.setType(Material.OAK_SIGN);
		final Sign sign = (Sign) signblock.getState();
		sign.setLine(1, playername);
		sign.update();
		loc.getWorld().getBlockAt(X, Y - 1, Z).setType(Material.STONE_BRICKS, false);
		loc.getWorld().getBlockAt(X, Y - 1, Z - 1).setType(Material.STONE_BRICKS, false);
	}

}