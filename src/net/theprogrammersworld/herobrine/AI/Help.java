package net.theprogrammersworld.herobrine.AI;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_14_R1.PacketPlayOutChat;

public class Help {
	public static void displayHelp(Player player) {
		// Display the help, listing only the commands that the user has permissions to
		// use.
		player.sendMessage(ChatColor.RED + "[Herobrine] Command List (hover over for more info)");

		IChatBaseComponent help = ChatSerializer.a(
				"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine help\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
						+ "§aShows this list of Herobrine commands\"}}]}");
		PacketPlayOutChat packet = new PacketPlayOutChat(help);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);

		if (player.hasPermission("herobrine.attack")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine attack <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aSends Herobrine to attack\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.haunt")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine haunt <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aSends Herobrine to haunt\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.cancel")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine cancel\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aCancels Herobrine's current target\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.reload")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine reload\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aReloads the Herobrine configuration file\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.position")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine position\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aDisplays Herobrine's coordinates\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.pyramid")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine pyramid <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aBuilds a pyramid\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.bury")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine bury <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aBuries the specified player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.graveyard")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine graveyard <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aTeleports the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.temple")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine temple <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aBuilds a temple near the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.heads")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine heads <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aSpawns heads near the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.cave")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine cave <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aCreates a cave near the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.burn")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine burn <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aBurns the specified player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.curse")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine curse <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aPlaces a curse on the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.speakrandom")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine speakrandom <player>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aSends a random Herobrine message to the player\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.speak")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine speak <message>\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aSends a chat message on Herobrine's behalf\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
		if (player.hasPermission("herobrine.allworlds")) {
			help = ChatSerializer.a(
					"{\"text\":\"\",\"extra\":[{\"text\":\"§a/herobrine allworlds\",\"hoverEvent\":{\"action\":\"show_text\",\"value\":\""
							+ "§aGrants Herobrine access to all worlds\"}}]}");
			packet = new PacketPlayOutChat(help);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}
}
