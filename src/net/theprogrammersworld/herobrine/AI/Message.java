package net.theprogrammersworld.herobrine.AI;

import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;

import org.bukkit.entity.Player;

public class Message {

	public static void sendRandomMessage(final Player player) {
		if (Herobrine.getPluginCore().getConfigDB().SendMessages) {
			int count = Herobrine.getPluginCore().getConfigDB().useMessages.size();
			final Random randgen = new Random();
			if (count == 0 || count < 0){
				count = 0;
			}
			final int randmsg = randgen.nextInt(count);
			player.sendMessage("<Herobrine> " + Herobrine.getPluginCore().getConfigDB().useMessages.get(randmsg));
		}
	}

}