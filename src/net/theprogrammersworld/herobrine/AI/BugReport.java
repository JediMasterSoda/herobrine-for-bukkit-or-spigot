package net.theprogrammersworld.herobrine.AI;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.theprogrammersworld.herobrine.ConfigDB;

public class BugReport {
	public static String sendBugReport(Player player, String[] messageWords) {
		// Collect the necessary information and POST it to the server where bug reports will be
		// collected. Based on the result of the submission, return a string.
		// (0 = report failed, 1 = unofficial version, 2 = unsupported version, Report ID => success)
		String report;
		String checksum;
		
		// Collect basic information including the UUID of the player who made the submission, the
		// IP address of the server, the port on which the server is running, and the plugin version.
		String serverIP = getServerIP();
		String playerUUID;
		try {
			playerUUID = player.getUniqueId().toString();
		} catch (Exception e) {
			playerUUID = "CONSOLE";
		}
		
		try {
			// Get the contents of the configuration file.
			File configFile = new File("plugins" + File.separator + "Herobrine" + File.separator + "config.yml");
			FileInputStream configFileInputStream = new FileInputStream(configFile);
			byte[] configFileBytes = new byte[(int) configFile.length()];
			configFileInputStream.read(configFileBytes);
			configFileInputStream.close();
			String configFileString = new String(configFileBytes, "UTF-8");
			
			report = "Server IP Address: " + serverIP + "\r\n" +
					"Submission UUID: " + playerUUID + "\r\n" +
					"Server Port: " + Bukkit.getServer().getPort() + "\r\n" +
					"Version: " + ConfigDB.pluginVersionNumber + "\r\n\r\n" +
					"Plugin Configuration File:\r\n" + configFileString + " \r\n\r\n";
			
			// Assemble the array of words in to a single message.
			for(int x = 1; x < messageWords.length; x++) {
				report += messageWords[x] + " ";
			}
			
			// Determine the MD5 hash of the plugin.
			checksum = computeMD5();
			
			// After all of the necessary information has been acquired, submit the report.
			try {
				String postData = URLEncoder.encode("report", "UTF-8") + "=" + URLEncoder.encode(report, "UTF-8") +
						"&" + URLEncoder.encode("checksum", "UTF-8") + "=" + URLEncoder.encode(checksum, "UTF-8") +
						"&" + URLEncoder.encode("ip", "UTF-8") + "=" + URLEncoder.encode(serverIP, "UTF-8") +
						"&" + URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(playerUUID, "UTF-8");
				URL submitURL = new URL("https://www.theprogrammersworld.net/Herobrine/pluginBugReporter.php");
				URLConnection urlConn = submitURL.openConnection();
				urlConn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
				wr.write(postData);
				wr.flush();
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
				String serverResponse = rd.readLine();
				rd.close();
				wr.close();
				return serverResponse;
			} catch (Exception e){}
		} catch (Exception e){}
		// The report failed because for some reason, the server could not be reached.
		return "0";
	}
	
	private static String getServerIP() {
		// Get the IP address of the server by calling a page on the website that will return the IP.
		URL ipFetchURL;
		try {
			ipFetchURL = new URL("https://www.theprogrammersworld.net/Herobrine/pluginIPFetcher.php");
			InputStreamReader ipFetchISR = new InputStreamReader(ipFetchURL.openStream());
			BufferedReader ipFetchBR = new BufferedReader(ipFetchISR);
			String ipAddress = ipFetchBR.readLine();
			ipFetchBR.close();
			ipFetchISR.close();
			return ipAddress;
		} catch (Exception e) {
			return "";
		}
	}
	
	private static String computeMD5() {
		// Determine the MD5 hash of the plugin.
		File pluginFile = new File(BugReport.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        try
        {
        	String filepath = URLDecoder.decode(pluginFile.getAbsolutePath(), StandardCharsets.UTF_8.toString());
            StringBuilder hash = new StringBuilder();
            MessageDigest md = MessageDigest.getInstance("MD5");
            FileInputStream fis = new FileInputStream(filepath);
            byte[] dataBytes = new byte[1024];
            int nread = 0; 

            while((nread = fis.read(dataBytes)) != -1)  
                 md.update(dataBytes, 0, nread);

            byte[] mdbytes = md.digest();

            for(int i=0; i<mdbytes.length; i++)
            hash.append(Integer.toString((mdbytes[i] & 0xff) + 0x100 , 16).substring(1));
            fis.close();
            return hash.toString();
        }
        catch(Exception e)
        {
        	return "";
        }
	}
}
