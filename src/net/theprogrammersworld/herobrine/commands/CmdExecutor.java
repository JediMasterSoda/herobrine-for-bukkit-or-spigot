package net.theprogrammersworld.herobrine.commands;

import java.util.logging.Logger;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.BugReport;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.AI.Message;
import net.theprogrammersworld.herobrine.AI.Help;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdExecutor implements CommandExecutor {

	private Herobrine P_Core;
	private Logger log;
	private ChatColor red = ChatColor.RED;

	public CmdExecutor(final Herobrine i) {
		super();
		P_Core = null;
		log = null;
		P_Core = i;
		log = Herobrine.log;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd,
			final String commandLabel, final String[] args) {
		if (sender instanceof Player) {
			final Player player = (Player) sender;
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("attack")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.attack")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkAttack(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										if (P_Core.canAttackPlayer(
												Bukkit.getServer().getPlayer(
														args[1]), player)) {
											if (!AICore.isTarget) {
												P_Core.getAICore()
														.setAttackTarget(
																Bukkit.getServer()
																		.getPlayer(
																				args[1]));
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is now attacking "
														+ args[1] + ".");
											} else {
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is already attacking "
														+ args[1]
														+ ". Use "
														+ ChatColor.GREEN
														+ "/herobrine cancel"
														+ ChatColor.RED
														+ " to stop the attack.");
											}
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " cannot be attacked because "
												+ "they are in a secure area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be attacked because they "
											+ "are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be attacked because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to initiate Herobrine "
									+ "attacks against other players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine attack <player>");
					}
				} else if (args[0].equalsIgnoreCase("pyramid")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.pyramid")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core.getAICore()
												.getCore(Core.CoreType.PYRAMID)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] Generating a pyramind near "
													+ args[1] + ".");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] A pyramid could not be generated near "
													+ args[1]
													+ " because there is no good place for it near them.");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] A pyramid could not be generated near "
												+ args[1]
												+ " because they are in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] A pyramid could not be generated near "
											+ args[1]
											+ " because they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] A pyramid could not be generated near "
										+ args[1]
										+ " because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to create Herobrine-"
									+ "built pyramids near players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine pyramid <player>");
					}
				} else if (args[0].equalsIgnoreCase("temple")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.temple")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (this.P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core.getAICore()
												.getCore(Core.CoreType.TEMPLE)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[HerobrineAI] Creating temple near "
													+ args[1] + "!");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[HerobrineAI] Cannot find good place for temple!");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[HerobrineAI] Player is in secure area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[HerobrineAI] Player is offline.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[HerobrineAI] Player is offline.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to create a temple!");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine temple <player>");
					}
				} else if (args[0].equalsIgnoreCase("bury")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.bury")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkBuild(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										final Object[] data = { Bukkit
												.getServer().getPlayer(args[1]) };
										if (P_Core
												.getAICore()
												.getCore(
														Core.CoreType.BURY_PLAYER)
												.runCore(data).getResult()) {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] Herobrine has buried "
													+ args[1] + ".");
										} else {
											player.sendMessage(ChatColor.RED
													+ "[Herobrine] "
													+ args[1]
													+ " could not be buried "
													+ "because there is no good place to bury them.");
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " could not be buried "
												+ "because they are are in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] " + args[1]
											+ " could not be buried because "
											+ "they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " could not be buried because "
										+ "they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to bury players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine bury <player>");
					}
				} else if (args[0].equalsIgnoreCase("cave")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.cave")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = {
										Bukkit.getServer().getPlayer(args[1])
												.getLocation(), true };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(
														Core.CoreType.BUILD_STUFF)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] A cave could not be created near "
										+ args[1]
										+ " because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to create caves near "
									+ "players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine cave <player>");
					}
				} else if (args[0].equalsIgnoreCase("burn")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.burn")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.BURN)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " could not be burned because they "
										+ "not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to tell Herobrine "
									+ "to burn players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine burn <player>");
					}
				} else if (args[0].equalsIgnoreCase("curse")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.curse")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.CURSE)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be cursed because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to tell Herobrine "
									+ "to curse players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine curse <player>");
					}
				} else if (args[0].equalsIgnoreCase("heads")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.heads")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								final Object[] data = { args[1] };
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ P_Core.getAICore()
												.getCore(Core.CoreType.HEADS)
												.runCore(data)
												.getResultString());
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ args[1]
										+ " cannot be haunted by heads because "
										+ "they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to haunt players "
									+ "with heads.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine heads <player>");
					}
				} else if (args[0].equalsIgnoreCase("position")) {
					if (player.hasPermission("herobrine.position")) {
						int x = (int) Herobrine.herobrineNPC.getNMSEntity().locX;
						int y = (int) Herobrine.herobrineNPC.getNMSEntity().locY;
						int z = (int) Herobrine.herobrineNPC.getNMSEntity().locZ;
						player.sendMessage(ChatColor.GREEN + "Herobrine's Coordinates: (" + x + ", " + y +
								", " + z + ")");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to view Herobrine's "
								+ "location.");
					}
				} else if (args[0].equalsIgnoreCase("graveyard")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.graveyard")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (!AICore.isTarget) {
										P_Core.getAICore().graveyardTeleport(
												Bukkit.getServer().getPlayer(
														args[1]));
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] " + args[1]
												+ " has been teleported to "
												+ "Herobrine's Graveyard.");
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] Another player is already in Herobrine's "
												+ "Graveyard. Use "
												+ ChatColor.GREEN
												+ "/herobrine cancel"
												+ ChatColor.RED
												+ " to teleport the current player out of the graveyard.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be teleported to "
											+ "Herobrine's Graveyard because they are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] "
										+ args[1]
										+ " cannot be teleported to "
										+ "Herobrine's Graveyard because they are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the permissions necessary to teleport players "
									+ "to Herobrine's Graveyard.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine graveyard <player>");
					}
				} else if (args[0].equalsIgnoreCase("haunt")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.haunt")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1])
										.isOnline()) {
									if (P_Core.getSupport().checkHaunt(
											Bukkit.getServer()
													.getPlayer(args[1])
													.getLocation())) {
										if (P_Core.canAttackPlayer(
												Bukkit.getServer().getPlayer(
														args[1]), player)) {
											if (!AICore.isTarget) {
												P_Core.getAICore()
														.setHauntTarget(
																Bukkit.getServer()
																		.getPlayer(
																				args[1]));
												String pname = args[1];
												Player target = Bukkit.getPlayer(pname);
												Herobrine.herobrineNPC.lookAtPoint(target.getLocation());
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is now haunting "
														+ args[1] + ".");
											} else {
												player.sendMessage(ChatColor.RED
														+ "[Herobrine] Herobrine is already haunting another player. Use "
														+ ChatColor.GREEN
														+ "/herobrine cancel"
														+ ChatColor.RED
														+ " to cancel the current haunting.");
											}
										}
									} else {
										player.sendMessage(ChatColor.RED
												+ "[Herobrine] "
												+ args[1]
												+ " cannot be haunted because they"
												+ " are currently in a protected area.");
									}
								} else {
									player.sendMessage(ChatColor.RED
											+ "[Herobrine] "
											+ args[1]
											+ " cannot be haunted because they "
											+ "are not online.");
								}
							} else {
								player.sendMessage(ChatColor.RED
										+ "[Herobrine] " + args[1]
										+ " cannot be haunted because they "
										+ "are not online.");
							}
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to initiate Herobrine "
									+ "hauntings.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine haunt <player>");
					}
				} else if (args[0].equalsIgnoreCase("speakrandom")) {
					if (args.length > 1) {
						if (player.hasPermission("herobrine.speakrandom")) {
							if (Bukkit.getServer().getPlayer(args[1]) != null) {
								if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
									Message.sendRandomMessage(Bukkit.getServer().getPlayer(args[1]));
								}
								else {
									player.sendMessage("Herobrine cannot send a random message to "
											+ args[1] + " because they are not online.");
								}
							}
							else {
								player.sendMessage("Herobrine cannot send a random message to "
										+ args[1] + " because they are not online.");
							}
						}
						else
						{
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to send random Herobrine "
									+ "messages to other players.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine speakrandom <player>");
					}
				} else if (args[0].equalsIgnoreCase("speak")) {
					if (args.length > 1) {
						if(player.hasPermission("herobrine.speak")) {
							String message = "";
							for(int x = 1; x < args.length; x++) {
								message += " " + args[x];
							}
							Bukkit.broadcastMessage("<Herobrine>" + message);
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to speak as Herobrine.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine speak <message>");
					}
				} else if (args[0].equalsIgnoreCase("pluginreport")) {
					if (args.length > 1) {
						if(player.hasPermission("herobrine.pluginreport")) {							
							String reportResponse = BugReport.sendBugReport(null, args);
							if(reportResponse.equals("0"))
								player.sendMessage(ChatColor.RED + "Your report was not was submitted because the report server could not " +
										"be reached. Please try again later.");
							else if(reportResponse.equals("1"))
								player.sendMessage(ChatColor.RED + "Your report was not submitted because this version of Herobrine does not " +
										"appear to be an official release. Please download an official version of the " +
										"plugin at: https://www.theprogrammersworld.net/Herobrine/download.php");
							else if(reportResponse.equals("2"))
								player.sendMessage(ChatColor.RED + "Your report was not submitted because this version of Herobrine is no " +
										"longer supported. Please download a newer version of the plugin at: " +
										"https://www.theprogrammersworld.net/Herobrine/download.php");
							else if(reportResponse.equals("4"))
								player.sendMessage(ChatColor.RED + "Your report was not submitted because this Minecraft server is banned " +
										"from submitting reports.");
							else if(reportResponse.equals("5"))
								player.sendMessage(ChatColor.RED + "Your report was not submitted because you are banned from submitting " +
										"reports.");
							else
								player.sendMessage(ChatColor.GREEN + "Your report was successfully submitted. For further assistance, please " +
										"post on our forum at https://www.theprogrammersworld.net/forum/ and " +
										"reference this report in your post with the following ID: " +
										reportResponse);
						} else {
							player.sendMessage(ChatColor.RED
									+ "You do not have the necessary permissions to submit Herobrine bug reports.");
						}
					} else {
						player.sendMessage(ChatColor.RED + "Usage: "
								+ ChatColor.GREEN
								+ "/herobrine pluginreport <message>");
					}
				} else if (args[0].equalsIgnoreCase("cancel")) {
					if (player.hasPermission("herobrine.cancel")) {
						P_Core.getAICore().cancelTarget(Core.CoreType.ANY, true);
						player.sendMessage(ChatColor.RED
								+ "[Herobrine] The current Herobrine victim has been saved.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to cancel Herobrine's "
								+ "actions.");
					}
				} else if (args[0].equalsIgnoreCase("reload")) {
					if (player.hasPermission("herobrine.reload")) {
						P_Core.getConfigDB().Reload();
						player.sendMessage(ChatColor.RED
								+ "[Herobrine] Herobrine configuration file reloaded.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to reload Herobrine's "
								+ "configuration file.");
					}
				} else if (args[0].equalsIgnoreCase("help")) {
					if (player.hasPermission("herobrine.help")) {
						Help.displayHelp(player);
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have the necessary permissions to view the Herobrine command documentation.");
					}
				} else if (args[0].equalsIgnoreCase("allworlds")) {
					if (player.hasPermission("herobrine.allworlds")) {
						Herobrine.getPluginCore().getConfigDB()
								.addAllWorlds();
						player.sendMessage(ChatColor.GREEN
								+ "[Herobrine] All worlds have been added to the configuration file. Herobrine can now access all of the server's worlds.");
						player.sendMessage(ChatColor.YELLOW
								+ "[Herobrine] WARNING! - One or more worlds was determined to have a space in its name. Please be aware that worlds with spaces in their name may cause problems.");
					} else {
						player.sendMessage(ChatColor.RED
								+ "You do no have the necessary permissions to add all worlds to the configuration file.");
					}
				} else {
					player.sendMessage(ChatColor.RED + "Usage: /herobrine help");
				}
			} else {
				player.sendMessage(ChatColor.RED + "Usage: /herobrine help");
			}
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("attack")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkAttack(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								if (P_Core.canAttackPlayerConsole(Bukkit
										.getServer().getPlayer(args[1]))) {
									if (!AICore.isTarget) {
										P_Core.getAICore().setAttackTarget(
												Bukkit.getServer().getPlayer(
														args[1]));
										log.info("[Herobrine] Herobrine is now attacking "
												+ args[1] + ".");
									} else {
										log.info("[Herobrine] Herobrine is already attacking "
												+ args[1]
												+ ". Use /herobrine cancel to stop the attack.");
									}
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " cannot be attacked because they are in a secure area.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be attacked because they are not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be attacked because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine attack <player>");
				}
			} else if (args[0].equalsIgnoreCase("pyramid")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data2 = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.PYRAMID)
										.runCore(data2).getResult()) {
									log.info("[Herobrine] Generating a pyramind near "
											+ args[1] + ".");
								} else {
									log.info("[Herobrine] A pyramid could not be generated near "
											+ args[1]
											+ " because there is no good place for it near them.");
								}
							} else {
								log.info("[Herobrine] A pyramid could not be generated near "
										+ args[1]
										+ " because they are in"
										+ " a protected area.");
							}
						} else {
							log.info("[Herobrine] A pyramid could not be generated near "
									+ args[1] + " because they are not online.");
						}
					} else {
						log.info("[Herobrine] A pyramid could not be generated near "
								+ args[1] + " because they are not online.");
					}
				} else {
					log.info("Usage: " + ChatColor.GREEN
							+ "/herobrine pyramid <player>");
				}
			} else if (args[0].equalsIgnoreCase("temple")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (this.P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.TEMPLE)
										.runCore(data).getResult()) {
									log.info("[HerobrineAI] Creating temple near "
											+ args[1] + "!");
								} else {
									log.info("[HerobrineAI] Cannot find good place for temple!");
								}
							} else {
								log.info("[HerobrineAI] " + args[1]
										+ " is in secure area.");
							}
						} else {
							log.info("[HerobrineAI] " + args[1]
									+ " is offline.");
						}
					} else {
						log.info("[HerobrineAI] " + args[1] + " is offline.");
					}
				} else {
					log.info("Usage: /herobrine temple <player>");
				}
			} else if (args[0].equalsIgnoreCase("bury")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkBuild(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								final Object[] data2 = { Bukkit.getServer()
										.getPlayer(args[1]) };
								if (P_Core.getAICore()
										.getCore(Core.CoreType.BURY_PLAYER)
										.runCore(data2).getResult()) {
									log.info("[Herobrine] Herobrine has buried "
											+ args[1] + ".");
								} else {
									log.info("[Herobrine] "
											+ args[1]
											+ " could not be buried "
											+ "because there is no good place to "
											+ "bury them.");
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " could not be buried "
										+ "because they are are in a protected area.");
							}
						} else {
							log.info("[Herobrine] " + args[1]
									+ " could not be buried because "
									+ "they are not online.");
						}
					} else {
						log.info("[Herobrine] " + args[1]
								+ " could not be buried because "
								+ "they are not online.");
					}
				} else {
					log.info("Usage: /herobrine bury <player>");
				}
			} else if (args[0].equalsIgnoreCase("cave")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = {
								Bukkit.getServer().getPlayer(args[1])
										.getLocation(), true };
						log.info(ChatColor.RED
								+ "[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.BUILD_STUFF)
										.runCore(data2).getResultString());
					} else {
						log.info(ChatColor.RED
								+ "[Herobrine] A cave could not be created near "
								+ args[1] + " because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine cave <player>");
				}
			} else if (args[0].equalsIgnoreCase("burn")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { Bukkit.getServer().getPlayer(
								args[1]) };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.BURN)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] " + args[1]
								+ " could not be burned because they "
								+ "not online.");
					}
				} else {
					log.info("Usage: /herobrine burn <player>");
				}
			} else if (args[0].equalsIgnoreCase("curse")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { Bukkit.getServer().getPlayer(
								args[1]) };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.CURSE)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] " + args[1]
								+ " cannot be cursed because they "
								+ "are not online.");
					}
				} else {
					log.info("Usage: /herobrine curse <player>");
				}
			} else if (args[0].equalsIgnoreCase("heads")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						final Object[] data2 = { args[1] };
						log.info("[Herobrine] "
								+ P_Core.getAICore()
										.getCore(Core.CoreType.HEADS)
										.runCore(data2).getResultString());
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be haunted by heads because they are not online.");
					}
				} else {
					log.info("Usage: " + ChatColor.GREEN
							+ "/herobrine heads <player>");
				}
			} else if (args[0].equalsIgnoreCase("graveyard")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (!AICore.isTarget) {
								P_Core.getAICore().graveyardTeleport(
										Bukkit.getServer().getPlayer(args[1]));
								log.info("[Herobrine] " + args[1]
										+ " is now in the Graveyard world!");
							} else {
								log.info(ChatColor.RED
										+ "[Herobrine] Another player is already in Herobrine's "
										+ "Graveyard. Use "
										+ ChatColor.GREEN
										+ "/herobrine cancel"
										+ ChatColor.RED
										+ " to teleport the current player out of the graveyard.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be teleported to Herobrine's Graveyard because they are "
									+ "not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be teleported to Herobrine's Graveyard because they are "
								+ "not online.");
					}
				} else {
					log.info("Usage: /herobrine graveyard <player>");
				}
			} else if (args[0].equalsIgnoreCase("haunt")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							if (P_Core.getSupport().checkHaunt(
									Bukkit.getServer().getPlayer(args[1])
											.getLocation())) {
								if (P_Core.canAttackPlayerConsole(Bukkit
										.getServer().getPlayer(args[1]))) {
									if (!AICore.isTarget) {
										P_Core.getAICore().setHauntTarget(
												Bukkit.getServer().getPlayer(
														args[1]));
										log.info("[Herobrine] Herobrine is now haunting "
												+ args[1] + ".");
									} else {
										log.info("[Herobrine] Herobrine is already haunting another player. Use /herobrine cancel to"
												+ " cancel the current haunting.");
									}
								}
							} else {
								log.info("[Herobrine] "
										+ args[1]
										+ " cannot be haunted because they are currently in a protected area.");
							}
						} else {
							log.info("[Herobrine] "
									+ args[1]
									+ " cannot be haunted because they are not online.");
						}
					} else {
						log.info("[Herobrine] "
								+ args[1]
								+ " cannot be haunted because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine haunt <player>");
				}
			} else if (args[0].equalsIgnoreCase("speakrandom")) {
				if (args.length > 1) {
					if (Bukkit.getServer().getPlayer(args[1]) != null) {
						if (Bukkit.getServer().getPlayer(args[1]).isOnline()) {
							Message.sendRandomMessage(Bukkit.getServer().getPlayer(args[1]));
						}
						else {
							log.info("[Herobrine] Herobrine cannot send a random message to "
									+ args[1] + " because they are not online.");
						}
					}
					else {
						log.info("[Herobrine] Herobrine cannot send a random message to "
								+ args[1] + " because they are not online.");
					}
				} else {
					log.info("Usage: /herobrine speakrandom <player>");
				}
			} else if (args[0].equalsIgnoreCase("speak")) {
				if (args.length > 1) {
					String message = "";
					for(int x = 1; x < args.length; x++) {
						message += " " + args[x];
					}
					Bukkit.broadcastMessage("<Herobrine>" + message);
				} else {
					log.info("Usage: /herobrine speak <message>");
				}
			} else if (args[0].equalsIgnoreCase("pluginreport")) {
				if (args.length > 1) {
					String reportResponse = BugReport.sendBugReport(null, args);
					if(reportResponse.equals("0"))
						log.info("Your report was not was submitted because the report server could not " +
								"be reached. Please try again later.");
					else if(reportResponse.equals("1"))
						log.info("Your report was not submitted because this version of Herobrine does not " +
								"appear to be an official release. Please download an official version of the " +
								"plugin at: https://www.theprogrammersworld.net/Herobrine/download.php");
					else if(reportResponse.equals("2"))
						log.info("Your report was not submitted because this version of Herobrine is no " +
								"longer supported. Please download a newer version of the plugin at: " +
								"https://www.theprogrammersworld.net/Herobrine/download.php");
					else if(reportResponse.equals("4"))
						log.info("Your report was not submitted because this Minecraft server is banned " +
								"from submitting reports.");
					else
						log.info("Your report was successfully submitted. For further assistance, please " +
								"post on our forum at https://www.theprogrammersworld.net/forum/ and " +
								"reference this report in your post with the following ID: " +
								reportResponse);
				} else {
					log.info("Usage: /herobrine pluginreport <message>");
				}
			} else if (args[0].equalsIgnoreCase("cancel")) {
				P_Core.getAICore().cancelTarget(Core.CoreType.ANY, true);
				log.info(ChatColor.RED
						+ "[Herobrine] The current Herobrine victim has been saved.");
			} else if (args[0].equalsIgnoreCase("reload")) {
				P_Core.getConfigDB().Reload();
				log.info("[Herobrine] Herobrine configuration file reloaded.");
			} else if (args[0].equalsIgnoreCase("position")) {
				double x = Herobrine.herobrineNPC.getNMSEntity().locX;
				double y = Herobrine.herobrineNPC.getNMSEntity().locY;
				double z = Herobrine.herobrineNPC.getNMSEntity().locZ;
				net.minecraft.server.v1_14_R1.World world = Herobrine.herobrineNPC.getNMSEntity().getWorld();
				sender.sendMessage(red + "Herobrine's location world: " + world + " x: " + x + " y: " + y +
						" z: " + z);

			} else if (args[0].equalsIgnoreCase("help")) {
				log.info("[Herobrine] Command List");
				log.info("/herobrine help - Shows this list of Herobrine commands");
				log.info("/herobrine attack <player> - Sends Herobrine to attack");
				log.info("/herobrine haunt <player> - Sends Herobrine to haunt");
				log.info("/herobrine cancel - Cancels Herobrine's current target");
				log.info("/herobrine reload - Reloads the Herobrine configuration file");
				log.info("/herobrine position - Displays Herobrine's coordinates");
				log.info("/herobrine pyramid <player> - Builds a pyramid");
				log.info("/herobrine bury <player> - Buries the specified player");
				log.info("/herobrine graveyard <player> - Teleports the player");
				log.info("/herobrine temple <player> - Builds a temple near player");
				log.info("/herobrine heads <player> - Spawns heads near player");
				log.info("/herobrine cave <player> - Creates a cave near the player");
				log.info("/herobrine burn <player> - Burns the specified player");
				log.info("/herobrine curse <player> - Places curse on the player");
				log.info("/herobrine speakrandom <player> - Sends a random Herobrine message to the player");
				log.info("/herobrine speak <message> - Sends a chat message on Herobrine's behalf");
				log.info("/herobrine allworlds - Grants Herobrine access to all worlds");
			} else if (args[0].equalsIgnoreCase("allworlds")) {
				Herobrine.getPluginCore().getConfigDB().addAllWorlds();
				log.info("[Herobrine] All worlds have been added to the configuration file. Herobrine can now access all of the server's worlds.");
				log.info("[Herobrine] WARNING! - One or more worlds was determined to have a space in its name. Please be aware that worlds with spaces in their name may cause problems.");
			} else {
				log.info("Usage: /herobrine help");
			}
		} else {
			log.info("Usage: /herobrine help");
		}
		return true;
	}

}