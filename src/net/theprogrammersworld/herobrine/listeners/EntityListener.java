package net.theprogrammersworld.herobrine.listeners;

import java.util.ArrayList;
import java.util.Random;

import net.theprogrammersworld.herobrine.Herobrine;
import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.Core;
import net.theprogrammersworld.herobrine.misc.ItemName;
import net.theprogrammersworld.herobrine.nms.entity.MobType;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;

public class EntityListener implements Listener {

	private ItemStack itemInHand;
	private ArrayList<String> equalsLore;
	private ArrayList<String> equalsLoreS;
	private ArrayList<String> getLore;
	
	public EntityListener(){
		equalsLore = new ArrayList<String>();
		equalsLoreS = new ArrayList<String>();
		equalsLore.add("Herobrine's Artifact");
		equalsLore.add("Bow of Teleporting");
		equalsLoreS.add("Herobrine's Artifact");
		equalsLoreS.add("Sword of Lightning");
	}
	
	@EventHandler
	public void onCreatureSpawn(final CreatureSpawnEvent event) {
		if (!Herobrine.isNPCDisabled && Herobrine.getPluginCore().getConfigDB().useWorlds.contains(event.getEntity().getLocation().getWorld().getName())) {
			final Entity entity = event.getEntity();
			final EntityType creatureType = event.getEntityType();
			if (event.isCancelled()) {
				return;
			}
			if (creatureType == EntityType.ZOMBIE) {
				if (Herobrine.getPluginCore().getConfigDB().UseNPC_Warrior && (new Random().nextInt(100) < Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Warrior.SpawnChance"))
						&& !Herobrine.getPluginCore().getEntityManager().isCustomMob(entity.getEntityId())) {
					final LivingEntity ent = (LivingEntity) entity;
					ent.setHealth(0);
					Herobrine.getPluginCore().getEntityManager().spawnCustomZombie(event.getLocation(), MobType.HEROBRINE_WARRIOR);
				}
			} else if ((creatureType == EntityType.SKELETON) && Herobrine.getPluginCore().getConfigDB().UseNPC_Demon
					&& (new Random().nextInt(100) < Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Demon.SpawnChance"))
					&& !Herobrine.getPluginCore().getEntityManager().isCustomMob(entity.getEntityId())) {
				final LivingEntity ent = (LivingEntity) entity;
				ent.setHealth(0);
				Herobrine.getPluginCore().getEntityManager().spawnCustomSkeleton(event.getLocation(), MobType.DEMON);
			}
		}
	}

	@EventHandler
	public void onEntityDeathEvent(final EntityDeathEvent event) {
		if (Herobrine.getPluginCore().getEntityManager().isCustomMob(event.getEntity().getEntityId())) {
			Herobrine.getPluginCore().getEntityManager().removeMob(event.getEntity().getEntityId());
		}
	}

	@EventHandler
	public void EntityTargetEvent(final EntityTargetLivingEntityEvent e) {
		final LivingEntity lv = e.getTarget();
		if (lv != null) {
			if (lv.getEntityId() == Herobrine.herobrineEntityID) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onEntityDamageByBlock(final EntityDamageByBlockEvent event) {
		if (event.getEntity().getEntityId() == Herobrine.herobrineEntityID) {
			event.setCancelled(true);
			event.setDamage(0);
		}
	}

	@EventHandler
	public void onEntityDamage(final EntityDamageEvent event) {		
		    if (event.getEntity().getEntityId() == Herobrine.herobrineEntityID)
		    {
		      if ((event instanceof EntityDamageByEntityEvent))
		      {
		        EntityDamageByEntityEvent dEvent = (EntityDamageByEntityEvent)event;
		        if ((Herobrine.getPluginCore().getConfigDB().Killable) && (Herobrine.getPluginCore().getAICore().getCoreTypeNow() != Core.CoreType.GRAVEYARD)) {
		          if ((dEvent.getDamager() instanceof Player))
		          {
		            if (event.getDamage() >= Herobrine.HerobrineHP)
		            {
		              int i = 1;
		              for (i = 1; i <= 2500; i++) {
		                if (Herobrine.getPluginCore().getConfigDB().config.contains("config.Drops." + Integer.toString(i)))
		                {
		                  Random randgen = new Random();
		                  int chance = randgen.nextInt(100);
		                  if (chance <= Herobrine.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".chance")) {
		                    //Herobrine.herobrineNPC.getBukkitEntity().getLocation().getWorld().dropItemNaturally(Herobrine.herobrineNPC.getBukkitEntity().getLocation(), new ItemStack(Material.getMaterial(i), Herobrine.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".count")));
		                  }
		                }
		              }
		              Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
		              Herobrine.HerobrineHP = Herobrine.HerobrineMaxHP;
		              Player player = (Player)dEvent.getDamager();
		              player.sendMessage("<Herobrine> " + Herobrine.getPluginCore().getConfigDB().DeathMessage);
		            }
		            else
		            {
		              Herobrine.HerobrineHP = (int)(Herobrine.HerobrineHP - event.getDamage());
		              Herobrine.herobrineNPC.hurtAnimation();
		              AICore.log.info("HIT: " + event.getDamage());
		            }
		          }
		          else if ((dEvent.getDamager() instanceof Projectile))
		          {
		            Arrow arrow = (Arrow)dEvent.getDamager();
		            if ((arrow.getShooter() instanceof Player))
		            {
		              if (Herobrine.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		              {
		                Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
		                Herobrine.getPluginCore().getAICore().setAttackTarget((Player)arrow.getShooter());
		              }
		              else if (event.getDamage() >= Herobrine.HerobrineHP)
		              {
		                int i = 1;
		                for (i = 1; i <= 2500; i++) {
		                  if (Herobrine.getPluginCore().getConfigDB().config.contains("config.Drops." + Integer.toString(i)))
		                  {
		                    Random randgen = new Random();
		                    int chance = randgen.nextInt(100);
		                    if (chance <= Herobrine.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".chance")) {
		                      //Herobrine.herobrineNPC.getBukkitEntity().getLocation().getWorld().dropItemNaturally(Herobrine.herobrineNPC.getBukkitEntity().getLocation(), new ItemStack(Material.getMaterial(i), Herobrine.getPluginCore().getConfigDB().config.getInt("config.Drops." + Integer.toString(i) + ".count")));
		                    }
		                  }
		                }
		                Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
		                Herobrine.HerobrineHP = Herobrine.HerobrineMaxHP;
		                Player player = (Player)arrow.getShooter();
		                player.sendMessage("<Herobrine> " + Herobrine.getPluginCore().getConfigDB().DeathMessage);
		              }
		              else
		              {
		                Herobrine.HerobrineHP = (int)(Herobrine.HerobrineHP - event.getDamage());
		                Herobrine.herobrineNPC.hurtAnimation();
		                AICore.log.info("HIT: " + event.getDamage());
		              }
		            }
		            else if (Herobrine.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		            {
		              Location newloc = Herobrine.herobrineNPC.getBukkitEntity().getLocation();
		              newloc.setY(-20.0D);
		              Herobrine.herobrineNPC.moveTo(newloc);
		              Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
		            }
		          }
		          else if (Herobrine.getPluginCore().getAICore().getCoreTypeNow() == Core.CoreType.RANDOM_POSITION)
		          {
		            Location newloc = Herobrine.herobrineNPC.getBukkitEntity().getLocation();
		            newloc.setY(-20.0D);
		            Herobrine.herobrineNPC.moveTo(newloc);
		            Herobrine.getPluginCore().getAICore().cancelTarget(Core.CoreType.ANY, true);
		          }
		        }else {
		        	event.setCancelled(true);
				      event.setDamage(0);
				      return;
		        }
		      }
		      
		    }
		    if ((event instanceof EntityDamageByEntityEvent))
		    {
		      EntityDamageByEntityEvent dEvent = (EntityDamageByEntityEvent)event;
		      if ((dEvent.getDamager() instanceof Player))
		      {
		        Player player = (Player)dEvent.getDamager();
		        if ((player.getInventory().getItemInMainHand()!= null) && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_SWORD) && (ItemName.getLore(player.getInventory().getItemInMainHand()) != null))
		        {
		          this.itemInHand = player.getInventory().getItemInMainHand();
		          this.getLore = ItemName.getLore(this.itemInHand);
		          if ((this.equalsLoreS != null) && (this.getLore.containsAll(this.equalsLoreS)) && (Herobrine.getPluginCore().getConfigDB().UseArtifactSword) && (new Random().nextBoolean())) {
		            player.getLocation().getWorld().strikeLightning(event.getEntity().getLocation());
		          }
		        }
		      }
		      else if ((dEvent.getDamager() instanceof Zombie))
		      {
		        Zombie zmb = (Zombie)dEvent.getDamager();
		        if ((zmb.getCustomName() == "Artifact Guardian") || (zmb.getCustomName() == "Herobrine´s Warrior")) {
		          dEvent.setDamage(dEvent.getDamage() * 3.0D);
		        }
		      }
		      else if ((dEvent.getDamager() instanceof Skeleton))
		      {
		        Skeleton zmb = (Skeleton)dEvent.getDamager();
		        if (zmb.getCustomName() == "Demon") {
		          dEvent.setDamage(dEvent.getDamage() * 3.0D);
		        }
		      }
		    }
		    if ((event.getCause() != null) && (event.getCause() == EntityDamageEvent.DamageCause.LIGHTNING) && ((event.getEntity() instanceof Player)) && (event.getEntity().getEntityId() != Herobrine.herobrineEntityID))
		    {
		      Player player = (Player)event.getEntity();
		      if ((player.getInventory().getItemInMainHand() != null) && (player.getInventory().getItemInMainHand().getType() == Material.DIAMOND_SWORD) && (ItemName.getLore(player.getInventory().getItemInMainHand()) != null))
		      {
		        this.itemInHand =player.getInventory().getItemInMainHand();;
		        this.getLore = ItemName.getLore(this.itemInHand);
		        if ((this.getLore.containsAll(this.equalsLoreS)) && (Herobrine.getPluginCore().getConfigDB().UseArtifactSword))
		        {
		          event.setDamage(0);
		          event.setCancelled(true);
		          return;
		        }
		      }
		    }
		  
	}

	@EventHandler
	  public void onProjectileHit(ProjectileHitEvent event) {
	    if ((event.getEntity() instanceof Arrow)) {
	      Arrow arrow = (Arrow)event.getEntity();
	      if ((arrow.getShooter() instanceof Player)) {
	        Player player = (Player)arrow.getShooter();
	        if (player.getInventory().getItemInMainHand() != null) {
	          this.itemInHand = player.getInventory().getItemInMainHand();
	          if ((this.itemInHand.getType() != null) && (this.itemInHand.getType() == Material.BOW)) {
	            this.getLore = ItemName.getLore(this.itemInHand);
	            if ((this.getLore != null) && (this.getLore.containsAll(this.equalsLore)) && (Herobrine.getPluginCore().getConfigDB().UseArtifactBow)) {
	              player.teleport(arrow.getLocation());
	            }
	          }
	        }
	      }
	    }
	  }
}