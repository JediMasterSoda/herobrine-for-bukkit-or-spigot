package net.theprogrammersworld.herobrine;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.theprogrammersworld.herobrine.AI.AICore;
import net.theprogrammersworld.herobrine.AI.extensions.GraveyardWorld;
import net.theprogrammersworld.herobrine.commands.CmdExecutor;
import net.theprogrammersworld.herobrine.listeners.BlockListener;
import net.theprogrammersworld.herobrine.listeners.EntityListener;
import net.theprogrammersworld.herobrine.listeners.InventoryListener;
import net.theprogrammersworld.herobrine.listeners.PlayerListener;
import net.theprogrammersworld.herobrine.listeners.WorldListener;
import net.theprogrammersworld.herobrine.nms.NPC.NPCCore;
import net.theprogrammersworld.herobrine.nms.NPC.entity.HumanNPC;
import net.theprogrammersworld.herobrine.nms.entity.EntityInjector;
import net.theprogrammersworld.herobrine.nms.entity.EntityManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

public class Herobrine extends JavaPlugin implements Listener {

	public static final Logger log = Bukkit.getLogger();

	private static Herobrine pluginCore;

	public static Herobrine getPluginCore() {
		return Herobrine.pluginCore;
	}

	private AICore aicore;
	private ConfigDB configdb;
	private Support support;
	private EntityManager entMng;
	private NPCCore NPCCore;
	public Map<Player, Long> PlayerApple = new HashMap<Player, Long>();
	public static HumanNPC herobrineNPC;
	public static long herobrineEntityID;
	public static int HerobrineHP = 200;
	public static int HerobrineMaxHP = 200;
	public static boolean availableWorld = false;
	public static boolean isNPCDisabled = false;

	public Location hbSpawnData = null;
	public boolean removeHBNextTick = false;

	@Override
	public void onLoad() {
		EntityInjector.inject();
	}

	@Override
	public void onEnable() {
		Herobrine.pluginCore = this;
		configdb = new ConfigDB(getLogger());
		aicore = new AICore();
		support = new Support();
		entMng = new EntityManager();
		NPCCore = new NPCCore();

		getServer().getPluginManager().registerEvents(this, this);
				
		// If the user has update checking turned on, start the thread responsible for performing
		// the check.
		if(configdb.CheckForUpdates)
		{
			new Thread(new UpdateScanner()).start();
		}
		
		Bukkit.getServer().getConsoleSender().sendMessage(
				ChatColor.GREEN + "Thank you for choosing the Herobrine plugin for " +
				"your Minecraft server. If you would like to promote your Minecraft server, please " +
				"donate to us at www.theprogrammersworld.net/donate.php and mention your server in " +
				"your donation message as well as why you like Herobrine. In return, we will add " +
				"your server and message to a randomly rotating server list on our plugin's " +
				"promotional pages. Thank you, and enjoy the plugin.");
	}

	public void onPostWorld() {
		configdb.Startup();
		configdb.Reload();
		
		getCommand("herobrine").setExecutor(new CmdExecutor(this));
		
		getServer().getPluginManager().registerEvents(new EntityListener(), this);
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new InventoryListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getServer().getPluginManager().registerEvents(new WorldListener(), this);

		initHerobrine();
	}

	private void initHerobrine() {
		if (configdb.UseGraveyardWorld && (Bukkit.getServer().getWorld(configdb.HerobrineWorldName) == null)) {
			Herobrine.log.info("[Herobrine] Creating Herobrine Graveyard world");
			final WorldCreator wc = new WorldCreator(configdb.HerobrineWorldName);
			wc.generateStructures(false);
			final WorldType type = WorldType.FLAT;
			wc.type(type);
			wc.createWorld();
			GraveyardWorld.create();
		}
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				if (Herobrine.this.removeHBNextTick) {
					Herobrine.this.removeHerobrine();
					Herobrine.this.spawnHerobrine(Herobrine.this.hbSpawnData);
					Herobrine.this.removeHBNextTick = false;
				}
			}
		}, 1L, 1L);
		final Location nowloc = new Location(Bukkit.getServer().getWorlds().get(0), 0.0, -20.0, 0.0);
		nowloc.setYaw(1.0f);
		nowloc.setPitch(1.0f);
		spawnHerobrine(nowloc);
		Herobrine.herobrineNPC.setItemInHand(configdb.ItemInHand.getItemStack());		     
	}

	@Override
	public void onDisable() {
		entMng.killAllMobs();
		aicore.stopBD();
		aicore.stopMAIN();
		aicore.stopRC();
		aicore.disableAll();
	}

	public InputStream getInputStreamData(final String src) {
		return Herobrine.class.getResourceAsStream(src);
	}

	public AICore getAICore() {
		return aicore;
	}

	public EntityManager getEntityManager() {
		return entMng;
	}

	public ConfigDB getConfigDB() {
		return configdb;
	}

	public Support getSupport() {
		return support;
	}

	public NPCCore getNPCCore() {
		return NPCCore;
	}
	
	public void spawnHerobrine(final Location loc) {
		Herobrine.herobrineNPC = NPCCore.spawnHumanNPC(loc);
		Herobrine.herobrineNPC.getBukkitEntity().setMetadata("NPC", new FixedMetadataValue(this, true));
		Herobrine.herobrineEntityID = Herobrine.herobrineNPC.getBukkitEntity().getEntityId();
	}
	
	

	public void removeHerobrine() {
		Herobrine.herobrineEntityID = 0L;
		Herobrine.herobrineNPC = null;
		NPCCore.removeAll();
	}

	public boolean canAttackPlayer(final Player player, final Player sender) {
		boolean opCheck = true;
		boolean creativeCheck = true;
		boolean ignoreCheck = true;
		if (!configdb.AttackOP && player.isOp()) {
			opCheck = false;
		}
		if (!configdb.AttackCreative && (player.getGameMode() == GameMode.CREATIVE)) {
			creativeCheck = false;
		}
		if (configdb.UseIgnorePermission && player.hasPermission("herobrine.ignore")) {
			ignoreCheck = false;
		}
		if (opCheck && creativeCheck && ignoreCheck) {
			return true;
		}
		if (!opCheck) {
			sender.sendMessage(ChatColor.RED + "[Herobrine] " + player.getDisplayName() + " is an OP.");
		} else if (!creativeCheck) {
			sender.sendMessage(ChatColor.RED + "[Herobrine] " + player.getDisplayName() + " is in creative mode.");
		} else if (!ignoreCheck) {
			sender.sendMessage(ChatColor.RED + "[Herobrine] " + player.getDisplayName() + " has ignore permissions.");
		}
		return false;
	}

	public boolean canAttackPlayerConsole(final Player player) {
		boolean opCheck = true;
		boolean creativeCheck = true;
		boolean ignoreCheck = true;
		if (!configdb.AttackOP && player.isOp()) {
			opCheck = false;
		}
		if (!configdb.AttackCreative && (player.getGameMode() == GameMode.CREATIVE)) {
			creativeCheck = false;
		}
		if (configdb.UseIgnorePermission && player.hasPermission("herobrine.ignore")) {
			ignoreCheck = false;
		}
		if (opCheck && creativeCheck && ignoreCheck) {
			return true;
		}
		if (!opCheck) {
			Herobrine.log.info("[Herobrine] " + player.getDisplayName() + " is an OP.");
		} else if (!creativeCheck) {
			Herobrine.log.info("[Herobrine] " + player.getDisplayName() + " is in creative mode.");
		} else if (!ignoreCheck) {
			Herobrine.log.info("[Herobrine] " + player.getDisplayName() + " has ignore permissions.");
		}
		return false;
	}

	public boolean canAttackPlayerNoMSG(final Player player) {
		boolean opCheck = true;
		boolean creativeCheck = true;
		boolean ignoreCheck = true;
		if (!configdb.AttackOP && player.isOp()) {
			opCheck = false;
		}
		if (!configdb.AttackCreative && (player.getGameMode() == GameMode.CREATIVE)) {
			creativeCheck = false;
		}
		if (configdb.UseIgnorePermission && player.hasPermission("herobrine.ignore")) {
			ignoreCheck = false;
		}
		return opCheck && creativeCheck && ignoreCheck;
	}

	public String getAvailableWorldString() {
		if (Herobrine.availableWorld) {
			return "Yes";
		}
		return "No";
	}

	public static boolean isSolidBlock(final Material mat) {
		return mat.isSolid();
	}

	public static boolean isAllowedBlock(final Material mat) {
		if(mat == Material.WATER || mat == Material.matchMaterial("STATIONARY_WATER") || mat == Material.LAVA || mat == Material.matchMaterial("STATIONARY_LAVA")
				|| mat == Material.ICE || mat == Material.PACKED_ICE)
			return false;
		else
			return mat.isSolid();
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		// When a player joins the server, perform several checks.
		Player joinedPlayer = event.getPlayer();
		
		// It is possible for a player to become stuck in Herobrine's Graveyard. If the player that
		// has just logged in is stuck in Herobrine's Graveyard, teleport them to the server's default
		// spawn point and let them know what happened.
		if(joinedPlayer.getWorld().getName().equals(configdb.HerobrineWorldName)) {
			List<World> worldList = Bukkit.getServer().getWorlds();
			joinedPlayer.teleport(worldList.get(0).getSpawnLocation());
			joinedPlayer.sendMessage(ChatColor.RED + "It looks like you got stuck in Herobrine's " +
					"Graveyard. This usually happens when the server has been shut off after you have been " +
					"teleported to it. To fix this, we have teleported you to this server's default spawn point.");
		}
		
		// Check if the player that has just joined is an OP. If they are, perform several additional checks.
		if(joinedPlayer.isOp()) {
			if(configdb.ShowDonateMsgToOp)
				// If the server is configured to display the donation promotion to OPs, then display it.
				joinedPlayer.sendMessage(ChatColor.GREEN + "Thank you for choosing the Herobrine plugin for " +
						"your Minecraft server. If you would like to promote your Minecraft server, please " +
						"donate to us at www.theprogrammersworld.net/donate.php and mention your server in " +
						"your donation message as well as why you like Herobrine. In return, we will add " +
						"your server and message to a randomly rotating server list on our plugin's " +
						"promotional pages. Thank you, and enjoy the plugin.");
			
				// If a newer version of Herobrine is available, display a message to the OP stating so.
				if(configdb.newVersionFound)
					joinedPlayer.sendMessage(ChatColor.RED + "A new version of Herobrine is available. To " +
							"get it, go to www.theprogrammersworld.net/Herobrine and click \"Download\".");
		}
	}

	@EventHandler
	public void onServerLoad(ServerLoadEvent event) {
		this.onPostWorld();
	}
}