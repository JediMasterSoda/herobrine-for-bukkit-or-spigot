package net.theprogrammersworld.herobrine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import net.theprogrammersworld.herobrine.AI.extensions.GraveyardWorld;
import net.theprogrammersworld.herobrine.misc.CustomID;

public class ConfigDB {
	private Logger log;
	public YamlConfiguration config;
	public YamlConfiguration npc;
	public boolean ShowDonateMsgToOp = true;
	public int ShowRate = 2;
	public boolean HitPlayer = true;
	public boolean SendMessages = true;
	public boolean Lightning = true;
	public boolean DestroyTorches = true;
	public int DestroyTorchesRadius = 5;
	public int ShowInterval = 144000;
	public boolean TotemExplodes = true;
	public boolean OnlyWalkingMode = false;
	public boolean BuildStuff = true;
	public boolean PlaceSigns = true;
	public boolean UseTotem = true;
	public boolean WriteBooks = true;
	public boolean Killable = false;
	public boolean UsePotionEffects = true;
	public int CaveChance = 40;
	public int BookChance = 5;
	public int SignChance = 5;
	public String DeathMessage = "You cannot kill me!";
	public List<String> useWorlds = new ArrayList<>();
	public List<String> useMessages = new ArrayList<>();
	public List<String> useSignMessages = new ArrayList<>();
	public List<String> useBookMessages = new ArrayList<>();
	public boolean BuildPyramids = true;
	public int BuildPyramidOnChunkPercentage = 5;
	public boolean UseGraveyardWorld = true;
	public boolean BuryPlayers = true;
	public boolean SpawnWolves = true;
	public boolean SpawnBats = true;
	public int WalkingModeXRadius = 1000;
	public int WalkingModeZRadius = 1000;
	public boolean BuildTemples = true;
	public int BuildTempleOnChunkPercentage = 5;
	public boolean UseArtifactBow = true;
	public boolean UseArtifactSword = true;
	public boolean UseArtifactApple = true;
	public boolean AttackCreative = true;
	public boolean AttackOP = true;
	public boolean SecuredArea_Attack = true;
	public boolean SecuredArea_Build = true;
	public boolean SecuredArea_Haunt = true;
	public boolean SecuredArea_Signs = true;
	public boolean SecuredArea_Books = true;
	public int HerobrineHP = 150;
	public int BuildInterval = 72000;
	public boolean UseHeads = true;
	public boolean UseAncientSword = true;
	public boolean UseNPC_Guardian = true;
	public boolean UseNPC_Warrior = true;
	public boolean UseNPC_Demon = true;
	public CustomID ItemInHand = null;
	public boolean Explosions = true;
	public boolean Burn = true;
	public boolean Curse = true;
	public int maxBooks = 1;
	public int maxSigns = 1;
	public int maxHeads = 1;
	public boolean UseIgnorePermission = true;
	public String HerobrineName = "Herobrine";
	public String HerobrineWorldName = "world_herobrine_graveyard";
	public String HerobrineUUID = "f84c6a79-0a4e-45e0-879b-cd49ebd4c4e2";
	public boolean UseSound = true;
	public boolean ShowInTabList = false;
	public boolean CheckForUpdates = true;

	public static String pluginVersionNumber = Bukkit.getServer().getPluginManager().getPlugin("Herobrine")
			.getDescription().getVersion();
	public boolean newVersionFound = false;
	private boolean isStartupDone = false;

	public ConfigDB(Logger l) {
		this.log = l;
	}

	public File configF = new File("plugins" + File.separator + "Herobrine" + File.separator + "config.yml");
	public File npcF = new File("plugins" + File.separator + "Herobrine" + File.separator + "npc.yml");

	public void Startup() {
		// Import plugin configuration from old HerobrineAI configuration.
		File herobrineAIFolder = new File("plugins" + File.separator + "HerobrineAI");
		if (herobrineAIFolder.exists()) {
			herobrineAIFolder.renameTo(new File("plugins" + File.separator + "Herobrine"));
		}

		new File("plugins" + File.separator + "Herobrine").mkdirs();
		if (!this.configF.exists()) {
			try {
				this.configF.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.config = new YamlConfiguration();
		if (!this.npcF.exists()) {
			try {
				this.npcF.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		this.npc = new YamlConfiguration();
		try {
			this.config.load(this.configF);
			this.npc.load(this.npcF);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		if (!this.npc.contains("npc.Guardian")) {
			this.npc.set("npc.Guardian.SpawnCount", Integer.valueOf(1));
			this.npc.set("npc.Guardian.HP", Integer.valueOf(40));
			this.npc.set("npc.Guardian.Speed", Double.valueOf(0.3D));
			this.npc.set("npc.Guardian.Drops.283.Chance", Integer.valueOf(40));
			this.npc.set("npc.Guardian.Drops.283.Count", Integer.valueOf(1));
			this.npc.set("npc.Guardian.Drops.286.Chance", Integer.valueOf(30));
			this.npc.set("npc.Guardian.Drops.286.Count", Integer.valueOf(1));
			this.npc.set("npc.Warrior.SpawnChance", Integer.valueOf(4));
			this.npc.set("npc.Warrior.HP", Integer.valueOf(40));
			this.npc.set("npc.Warrior.Speed", Double.valueOf(0.3D));
			this.npc.set("npc.Warrior.Drops.307.Chance", Integer.valueOf(25));
			this.npc.set("npc.Warrior.Drops.307.Count", Integer.valueOf(1));
			this.npc.set("npc.Warrior.Drops.306.Chance", Integer.valueOf(20));
			this.npc.set("npc.Warrior.Drops.306.Count", Integer.valueOf(1));
			this.npc.set("npc.Demon.SpawnChance", Integer.valueOf(4));
			this.npc.set("npc.Demon.HP", Integer.valueOf(40));
			this.npc.set("npc.Demon.Speed", Double.valueOf(0.3D));
			this.npc.set("npc.Demon.Drops.322.Chance", Integer.valueOf(40));
			this.npc.set("npc.Demon.Drops.322.Count", Integer.valueOf(1));
			this.npc.set("npc.Demon.Drops.397.Chance", Integer.valueOf(20));
			this.npc.set("npc.Demon.Drops.397.Count", Integer.valueOf(1));
			try {
				this.npc.save(this.npcF);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (!this.config.contains("config.ShowRate")) {
			this.useWorlds.add("world");
			this.useMessages.add("Even Notch can't save you now!");
			this.useMessages.add("Fear me!");
			this.useMessages.add("Welcome to my world!");
			this.useMessages.add("I am your death!");
			this.useMessages.add("Grave awaits you!");
			this.useSignMessages.add("I'm watching.");
			this.useSignMessages.add("Death...");
			this.useSignMessages.add("Eyes in dark...");
			this.useBookMessages.add("White eyes in dark...");
			this.useBookMessages.add("... was last what I saw ...");
			this.useBookMessages.add("... before i was dead.");

			this.log.info("[Herobrine] Creating new Config ...");
			this.config.set("config.ShowInterval", Integer.valueOf(144000));
			this.config.set("config.ShowDonateMsgToOp", Boolean.valueOf(true));
			this.config.set("config.ShowRate", Integer.valueOf(2));
			this.config.set("config.HitPlayer", Boolean.valueOf(true));
			this.config.set("config.SendMessages", Boolean.valueOf(true));
			this.config.set("config.Lightning", Boolean.valueOf(false));
			this.config.set("config.DestroyTorches", Boolean.valueOf(true));
			this.config.set("config.DestroyTorchesRadius", Integer.valueOf(5));
			this.config.set("config.Worlds", this.useWorlds);
			this.config.set("config.TotemExplodes", Boolean.valueOf(true));
			this.config.set("config.OnlyWalkingMode", Boolean.valueOf(false));
			this.config.set("config.BuildStuff", Boolean.valueOf(true));
			this.config.set("config.PlaceSigns", Boolean.valueOf(true));
			this.config.set("config.UseTotem", Boolean.valueOf(true));
			this.config.set("config.WriteBooks", Boolean.valueOf(true));
			this.config.set("config.Killable", Boolean.valueOf(false));
			this.config.set("config.UsePotionEffects", Boolean.valueOf(true));
			this.config.set("config.CaveChance", Integer.valueOf(40));
			this.config.set("config.BookChance", Integer.valueOf(5));
			this.config.set("config.SignChance", Integer.valueOf(5));
			this.config.set("config.DeathMessage", "You cannot kill me!");
			this.config.set("config.Messages", this.useMessages);
			this.config.set("config.SignMessages", this.useSignMessages);
			this.config.set("config.BookMessages", this.useBookMessages);
			this.config.set("config.Drops.264.count", Integer.valueOf(1));
			this.config.set("config.Drops.264.chance", Integer.valueOf(20));
			this.config.set("config.BuildPyramids", Boolean.valueOf(true));
			this.config.set("config.BuildPyramidOnChunkPercentage", Integer.valueOf(5));
			this.config.set("config.UseGraveyardWorld", Boolean.valueOf(true));
			this.config.set("config.BuryPlayers", Boolean.valueOf(true));
			this.config.set("config.SpawnWolves", Boolean.valueOf(true));
			this.config.set("config.SpawnBats", Boolean.valueOf(true));
			this.config.set("config.WalkingModeRadius.X", Integer.valueOf(1000));
			this.config.set("config.WalkingModeRadius.Z", Integer.valueOf(1000));
			this.config.set("config.BuildInterval", Integer.valueOf(72000));
			this.config.set("config.BuildTemples", Boolean.valueOf(true));
			this.config.set("config.BuildTempleOnChunkPercentage", Integer.valueOf(5));
			this.config.set("config.UseArtifacts.Bow", Boolean.valueOf(true));
			this.config.set("config.UseArtifacts.Sword", Boolean.valueOf(true));
			this.config.set("config.UseArtifacts.Apple", Boolean.valueOf(true));
			this.config.set("config.HerobrineHP", Integer.valueOf(300));
			this.config.set("config.AttackCreative", Boolean.valueOf(true));
			this.config.set("config.AttackOP", Boolean.valueOf(true));
			this.config.set("config.SecuredArea.Build", Boolean.valueOf(true));
			this.config.set("config.SecuredArea.Attack", Boolean.valueOf(true));
			this.config.set("config.SecuredArea.Haunt", Boolean.valueOf(true));
			this.config.set("config.SecuredArea.Signs", Boolean.valueOf(true));
			this.config.set("config.SecuredArea.Books", Boolean.valueOf(true));
			this.config.set("config.UseHeads", Boolean.valueOf(true));
			this.config.set("config.UseAncientSword", Boolean.valueOf(true));
			this.config.set("config.UseNPC.Guardian", Boolean.valueOf(true));
			this.config.set("config.UseNPC.Warrior", Boolean.valueOf(true));
			this.config.set("config.UseNPC.Demon", Boolean.valueOf(true));
			this.config.set("config.ItemInHand", "0");
			this.config.set("config.Explosions", Boolean.valueOf(true));
			this.config.set("config.Burn", Boolean.valueOf(true));
			this.config.set("config.Curse", Boolean.valueOf(true));
			this.config.set("config.Limit.Books", Integer.valueOf(1));
			this.config.set("config.Limit.Signs", Integer.valueOf(1));
			this.config.set("config.Limit.Heads", Integer.valueOf(1));
			this.config.set("config.UseIgnorePermission", Boolean.valueOf(false));
			this.config.set("config.Name", "Herobrine");
			this.config.set("config.HerobrineWorldName", "world_herobrine_graveyard");
			this.config.set("config.HerobrineUUID", "f84c6a79-0a4e-45e0-879b-cd49ebd4c4e2");
			this.config.set("config.UseHauntSound", Boolean.valueOf(true));
			this.config.set("config.ShowInTabList", Boolean.valueOf(false));
			this.config.set("config.CheckForUpdates", Boolean.valueOf(true));
			try {
				this.config.save(this.configF);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			boolean hasUpdated = false;
			if (!this.config.contains("config.HerobrineWorldName")) {
				hasUpdated = true;
				this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				this.config.set("config.HerobrineWorldName", this.HerobrineWorldName);
			}
			if (!this.config.contains("config.Worlds")) {
				hasUpdated = true;
				this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				this.config.set("config.Worlds", this.useWorlds);
			}
			if (!this.config.contains("config.TotemExplodes")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.TotemExplodes", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.OnlyWalkingMode")) {
				if (!hasUpdated) {
					hasUpdated = true;
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				this.config.set("config.OnlyWalkingMode", Boolean.valueOf(false));
			}
			if (!this.config.contains("config.BuildStuff")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.useMessages.add("Even Notch can't save you now!");
				this.useMessages.add("Fear me!");
				this.useMessages.add("Welcome to my world!");
				this.useMessages.add("I am your death!");
				this.useMessages.add("Grave awaits you!");
				this.useSignMessages.add("I'm watching.");
				this.useSignMessages.add("Death...");
				this.useSignMessages.add("Eyes in dark...");
				this.useBookMessages.add("White eyes in dark...");
				this.useBookMessages.add("... was last what I saw ...");
				this.useBookMessages.add("... before i was dead.");

				this.config.set("config.BuildStuff", Boolean.valueOf(true));
				this.config.set("config.PlaceSigns", Boolean.valueOf(true));
				this.config.set("config.UseTotem", Boolean.valueOf(true));
				this.config.set("config.WriteBooks", Boolean.valueOf(true));
				this.config.set("config.Killable", Boolean.valueOf(false));
				this.config.set("config.Messages", this.useMessages);
				this.config.set("config.SignMessages", this.useSignMessages);
				this.config.set("config.BookMessages", this.useBookMessages);
				this.config.set("config.Drops.264.count", Integer.valueOf(1));
				this.config.set("config.Drops.264.chance", Integer.valueOf(20));
			}
			if (!this.config.contains("config.UsePotionEffects")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.UsePotionEffects", Boolean.valueOf(true));
				this.config.set("config.CaveChance", Integer.valueOf(40));
				this.config.set("config.BookChance", Integer.valueOf(5));
				this.config.set("config.SignChance", Integer.valueOf(5));
			}
			if (!this.config.contains("config.DeathMessage")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.DeathMessage", "You cannot kill me!");
			}
			if (!this.config.contains("config.BuildPyramids")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.BuildPyramids", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.UseGraveyardWorld")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.UseGraveyardWorld", Boolean.valueOf(true));
				this.config.set("config.BuryPlayers", Boolean.valueOf(true));
				this.config.set("config.SpawnWolves", Boolean.valueOf(true));
				this.config.set("config.SpawnBats", Boolean.valueOf(true));
				if (this.config.contains("config.UsePoitonEffects")) {
					this.config.set("config.UsePoitonEffects", null);
				}
			}
			if (!this.config.contains("config.DeathMessage")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.DeathMessage", "You cannot kill me!");
			}
			if (!this.config.contains("config.WalkingModeRadius.X")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.WalkingModeRadius.X", Integer.valueOf(1000));
				this.config.set("config.WalkingModeRadius.Z", Integer.valueOf(1000));
			}
			if (!this.config.contains("config.BuildTemples")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.BuildInterval", Integer.valueOf(45000));
				this.config.set("config.BuildTemples", Boolean.valueOf(true));
				this.config.set("config.UseArtifacts.Bow", Boolean.valueOf(true));
				this.config.set("config.UseArtifacts.Sword", Boolean.valueOf(true));
				this.config.set("config.UseArtifacts.Apple", Boolean.valueOf(true));
				this.config.set("config.HerobrineHP", Integer.valueOf(200));
				this.config.set("config.AttackCreative", Boolean.valueOf(true));
				this.config.set("config.AttackOP", Boolean.valueOf(true));
				this.config.set("config.SecuredArea.Build", Boolean.valueOf(true));
				this.config.set("config.SecuredArea.Attack", Boolean.valueOf(true));
				this.config.set("config.SecuredArea.Haunt", Boolean.valueOf(true));
				this.config.set("config.SecuredArea.Signs", Boolean.valueOf(true));
				this.config.set("config.SecuredArea.Books", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.UseHeads")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;
				this.config.set("config.UseHeads", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.UseAncientSword")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.UseAncientSword", Boolean.valueOf(true));
				this.config.set("config.UseNPC.Guardian", Boolean.valueOf(true));
				this.config.set("config.UseNPC.Warrior", Boolean.valueOf(true));
				this.config.set("config.ItemInHand", "0");
			}
			if (!this.config.contains("config.Explosions")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.Explosions", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.UseNPC.Demon")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.UseNPC.Demon", Boolean.valueOf(true));
				this.config.set("config.Burn", Boolean.valueOf(true));
				this.config.set("config.Curse", Boolean.valueOf(true));
			}
			if (!this.npc.contains("npc.Warrior.Speed")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.npc.set("npc.Warrior.Speed", Double.valueOf(0.3D));
				this.npc.set("npc.Guardian.Speed", Double.valueOf(0.3D));
				this.npc.set("npc.Demon.Speed", Double.valueOf(0.3D));
			}
			if (!this.config.contains("config.Limit.Books")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.Limit.Books", Integer.valueOf(1));
				this.config.set("config.Limit.Signs", Integer.valueOf(1));
				this.config.set("config.Limit.Heads", Integer.valueOf(1));
			}
			if (!this.config.contains("config.UseIgnorePermission")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.UseIgnorePermission", Boolean.valueOf(false));
			}
			if (!this.config.contains("config.Name")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.Name", "Herobrine");
			}
			if (!this.config.contains("config.HerobrineUUID")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.HerobrineUUID", "f84c6a79-0a4e-45e0-879b-cd49ebd4c4e2");
			}
			if (!this.config.contains("config.UseHauntSound")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.UseHauntSound", Boolean.valueOf(true));
			}
			if (!this.config.contains("config.ShowInTabList")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.ShowInTabList", Boolean.valueOf(false));
			}
			if (!this.config.contains("config.CheckForUpdates")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.CheckForUpdates", Boolean.valueOf(true));
			}
			if (this.config.contains("config.UseWalkingMode")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.UseWalkingMode", null);
				this.config.set("config.WalkingModeRadius.FromX", null);
				this.config.set("config.WalkingModeRadius.FromZ", null);
				this.config.set("config.BuildPyramidOnChunkPercentage", Integer.valueOf(5));
				this.config.set("config.BuildTempleOnChunkPercentage", Integer.valueOf(5));
			}
			if (!this.config.contains("config.ShowDonateMsgToOp")) {
				if (!hasUpdated) {
					this.log.info("[Herobrine] Configuration file updating to Herobrine v" + pluginVersionNumber);
				}
				hasUpdated = true;

				this.config.set("config.ShowDonateMsgToOp", Boolean.valueOf(true));
			}
			if (hasUpdated) {
				try {
					this.config.save(this.configF);
					this.npc.save(this.npcF);
				} catch (IOException e) {
					e.printStackTrace();
				}
				this.log.info("[Herobrine] The configuration file update completed successfully.");
			}
			Reload();
		}
	}

	public void Reload() {
		Herobrine.getPluginCore().removeHerobrine();
		World world = Bukkit.getServer().getWorld(this.HerobrineWorldName);
		if (this.UseGraveyardWorld && world != null) {
			Bukkit.getServer().unloadWorld(world, true);
		}
		try {
			this.config.load(this.configF);
			this.npc.load(this.npcF);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		this.ShowInterval = this.config.getInt("config.ShowInterval");
		this.ShowRate = this.config.getInt("config.ShowRate");
		this.HitPlayer = this.config.getBoolean("config.HitPlayer");
		this.SendMessages = this.config.getBoolean("config.SendMessages");

		// To maintain compatibility with older configuration files that had "Lightning"
		// misspelled as "Lighting", check if the configuration contains a "Lighting"
		// argument, and if it does, use it instead.
		if (this.config.contains("config.Lighting")) {
			this.Lightning = this.config.getBoolean("config.Lighting");
		} else {
			this.Lightning = this.config.getBoolean("config.Lightning");
		}

		this.DestroyTorches = this.config.getBoolean("config.DestroyTorches");
		this.DestroyTorchesRadius = this.config.getInt("config.DestroyTorchesRadius");
		this.useWorlds = this.config.getStringList("config.Worlds");
		this.TotemExplodes = this.config.getBoolean("config.TotemExplodes");
		this.OnlyWalkingMode = this.config.getBoolean("config.OnlyWalkingMode");
		this.BuildStuff = this.config.getBoolean("config.BuildStuff");
		this.PlaceSigns = this.config.getBoolean("config.PlaceSigns");
		this.UseTotem = this.config.getBoolean("config.UseTotem");
		this.WriteBooks = this.config.getBoolean("config.WriteBooks");
		this.Killable = this.config.getBoolean("config.Killable");
		this.UsePotionEffects = this.config.getBoolean("config.UsePotionEffects");
		this.CaveChance = this.config.getInt("config.CaveChance");
		this.BookChance = this.config.getInt("config.BookChance");
		this.SignChance = this.config.getInt("config.SignChance");
		this.DeathMessage = this.config.getString("config.DeathMessage");
		this.useMessages = this.config.getStringList("config.Messages");
		this.useSignMessages = this.config.getStringList("config.SignMessages");
		this.useBookMessages = this.config.getStringList("config.BookMessages");
		this.BuildPyramids = this.config.getBoolean("config.BuildPyramids");
		this.BuildPyramidOnChunkPercentage = this.config.getInt("config.BuildPyramidOnChunkPercentage");
		this.UseGraveyardWorld = this.config.getBoolean("config.UseGraveyardWorld");
		this.BuryPlayers = this.config.getBoolean("config.BuryPlayers");
		this.SpawnWolves = this.config.getBoolean("config.SpawnWolves");
		this.SpawnBats = this.config.getBoolean("config.SpawnBats");
		this.WalkingModeXRadius = this.config.getInt("config.WalkingModeRadius.X");
		this.WalkingModeZRadius = this.config.getInt("config.WalkingModeRadius.Z");
		this.BuildInterval = this.config.getInt("config.BuildInterval");
		this.BuildTemples = this.config.getBoolean("config.BuildTemples");
		this.BuildTempleOnChunkPercentage = this.config.getInt("config.BuildTempleOnChunkPercentage");
		this.UseArtifactBow = this.config.getBoolean("config.UseArtifacts.Bow");
		this.UseArtifactSword = this.config.getBoolean("config.UseArtifacts.Sword");
		this.UseArtifactApple = this.config.getBoolean("config.UseArtifacts.Apple");
		this.HerobrineHP = this.config.getInt("config.HerobrineHP");
		this.AttackCreative = this.config.getBoolean("config.AttackCreative");
		this.AttackOP = this.config.getBoolean("config.AttackOP");
		this.SecuredArea_Build = this.config.getBoolean("config.SecuredArea.Build");
		this.SecuredArea_Attack = this.config.getBoolean("config.SecuredArea.Attack");
		this.SecuredArea_Haunt = this.config.getBoolean("config.SecuredArea.Haunt");
		this.SecuredArea_Signs = this.config.getBoolean("config.SecuredArea.Signs");
		this.SecuredArea_Books = this.config.getBoolean("config.SecuredArea.Books");
		this.UseHeads = this.config.getBoolean("config.UseHeads");
		this.UseAncientSword = this.config.getBoolean("config.UseAncientSword");
		this.UseNPC_Guardian = this.config.getBoolean("config.UseNPC.Guardian");
		this.UseNPC_Warrior = this.config.getBoolean("config.UseNPC.Warrior");
		this.UseNPC_Demon = this.config.getBoolean("config.UseNPC.Demon");
		this.ItemInHand = new CustomID(this.config.getString("config.ItemInHand"));
		this.Explosions = this.config.getBoolean("config.Explosions");
		this.Burn = this.config.getBoolean("config.Burn");
		this.Curse = this.config.getBoolean("config.Curse");
		this.maxBooks = this.config.getInt("config.Limit.Books");
		this.maxSigns = this.config.getInt("config.Limit.Signs");
		this.maxHeads = this.config.getInt("config.Limit.Heads");
		this.UseIgnorePermission = this.config.getBoolean("config.UseIgnorePermission");
		this.HerobrineName = this.config.getString("config.Name");
		this.HerobrineWorldName = this.config.getString("config.HerobrineWorldName");
		this.HerobrineUUID = this.config.getString("config.HerobrineUUID");
		this.UseSound = this.config.getBoolean("config.UseHauntSound");
		this.ShowInTabList = this.config.getBoolean("config.ShowInTabList");
		this.CheckForUpdates = this.config.getBoolean("config.CheckForUpdates");
		this.ShowDonateMsgToOp = this.config.getBoolean("config.ShowDonateMsgToOp");
		Herobrine.HerobrineMaxHP = this.HerobrineHP;
		Herobrine.getPluginCore().getAICore().stopMAIN();
		Herobrine.getPluginCore().getAICore().startMAIN();
		Herobrine.getPluginCore().getAICore().stopBD();
		Herobrine.getPluginCore().getAICore().startBD();
		Herobrine.getPluginCore().getAICore().stopRC();
		Herobrine.getPluginCore().getAICore().startRC();
		Herobrine.availableWorld = false;
		Herobrine.getPluginCore().getAICore().getResetLimits().updateFromConfig();
		if (Herobrine.herobrineNPC != null) {
			Herobrine.herobrineNPC.setItemInHand(this.ItemInHand.getItemStack());
		}
		if (this.isStartupDone) {
			world = Bukkit.getServer().getWorld(this.HerobrineWorldName);
			if (this.UseGraveyardWorld && world == null) {
				Herobrine.log.info("[Herobrine] Reloading Herobrine Graveyard world");
				final WorldCreator wc = new WorldCreator(this.HerobrineWorldName);
				wc.generateStructures(false);
				final WorldType type = WorldType.FLAT;
				wc.type(type);
				wc.createWorld();
				GraveyardWorld.create();
			}

			Herobrine.getPluginCore().spawnHerobrine(new Location(Bukkit.getWorlds().get(0), 0.0D, -20.0D, 0.0D));
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Herobrine.getPluginCore(), new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i <= ConfigDB.this.useWorlds.size() - 1; i++) {
						if (Bukkit.getServer().getWorlds()
								.contains(Bukkit.getServer().getWorld(ConfigDB.this.useWorlds.get(i)))) {
							Herobrine.availableWorld = true;
						}
					}
					if (!Herobrine.availableWorld) {
						ConfigDB.this.log.warning("[Herobrine] There are no available worlds for Herobrine.");
					}
				}
			}, 1L);
		}
		this.isStartupDone = true;
	}

	public void addAllWorlds() {
		ArrayList<String> allWorlds = new ArrayList<>();
		List<World> worlds_ = Bukkit.getWorlds();
		for (int i = 0; i <= worlds_.size() - 1; i++) {
			if (!worlds_.get(i).getName().equalsIgnoreCase(this.HerobrineWorldName)) {
				allWorlds.add(worlds_.get(i).getName());
			}
		}
		try {
			this.config.load(this.configF);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		this.config.set("config.Worlds", allWorlds);
		try {
			this.config.save(this.configF);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Reload();
	}
}
