package net.theprogrammersworld.herobrine.nms.entity;

public interface CustomEntity {

	void killCustom();

	MobType getMobType();

}
