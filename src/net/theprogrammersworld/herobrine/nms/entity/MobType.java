package net.theprogrammersworld.herobrine.nms.entity;

public enum MobType {
	ARTIFACT_GUARDIAN, HEROBRINE_WARRIOR, DEMON;
}