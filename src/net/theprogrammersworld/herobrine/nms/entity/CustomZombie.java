package net.theprogrammersworld.herobrine.nms.entity;

import java.util.Random;

import net.minecraft.server.v1_14_R1.ChatComponentText;
import net.minecraft.server.v1_14_R1.Entity;
import net.minecraft.server.v1_14_R1.EntityTypes;
import net.minecraft.server.v1_14_R1.EntityZombie;
import net.minecraft.server.v1_14_R1.GenericAttributes;
import net.minecraft.server.v1_14_R1.World;
import net.theprogrammersworld.herobrine.Herobrine;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

public class CustomZombie extends EntityZombie implements CustomEntity {

	private MobType mobType;

	public CustomZombie(EntityTypes<? extends Entity> entitytypes, World world) {
		super(EntityTypes.ZOMBIE, world);
	}

	public CustomZombie(final World world, final Location loc, final MobType mbt) {
		super(EntityTypes.ZOMBIE, world);
		mobType = null;
		mobType = mbt;
		if (mbt == MobType.ARTIFACT_GUARDIAN) {
			spawnArtifactGuardian(loc);
		} else if (mbt == MobType.HEROBRINE_WARRIOR) {
			spawnHerobrineWarrior(loc);
		}
	}

	private void spawnArtifactGuardian(final Location loc) {
		getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(Herobrine.getPluginCore().getConfigDB().npc.getDouble("npc.Guardian.Speed"));
		getAttributeInstance(GenericAttributes.MAX_HEALTH).setValue(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Guardian.HP"));
		setHealth(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Guardian.HP"));
		setCustomName(new ChatComponentText("Artifact Guardian"));
		((Zombie) getBukkitEntity()).getEquipment().setItemInMainHand(new ItemStack(Material.GOLDEN_SWORD, 1));
		((Zombie) getBukkitEntity()).getEquipment().setHelmet(new ItemStack(Material.GOLDEN_HELMET, 1));
		((Zombie) getBukkitEntity()).getEquipment().setChestplate(new ItemStack(Material.GOLDEN_CHESTPLATE, 1));
		((Zombie) getBukkitEntity()).getEquipment().setLeggings(new ItemStack(Material.GOLDEN_LEGGINGS, 1));
		((Zombie) getBukkitEntity()).getEquipment().setBoots(new ItemStack(Material.GOLDEN_BOOTS, 1));
		getBukkitEntity().teleport(loc);
	}

	private void spawnHerobrineWarrior(final Location loc) {
		getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(Herobrine.getPluginCore().getConfigDB().npc.getDouble("npc.Warrior.Speed"));
		getAttributeInstance(GenericAttributes.MAX_HEALTH).setValue(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Warrior.HP"));
		setHealth(Herobrine.getPluginCore().getConfigDB().npc.getInt("npc.Warrior.HP"));
		setCustomName(new ChatComponentText("Herobrine's Warrior"));
		((Zombie) getBukkitEntity()).getEquipment().setItemInMainHand(new ItemStack(Material.IRON_SWORD, 1));
		((Zombie) getBukkitEntity()).getEquipment().setHelmet(new ItemStack(Material.IRON_HELMET, 1));
		((Zombie) getBukkitEntity()).getEquipment().setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1));
		((Zombie) getBukkitEntity()).getEquipment().setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
		((Zombie) getBukkitEntity()).getEquipment().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
		getBukkitEntity().teleport(loc);
	}

	public CustomZombie(final World world) {
		super(EntityTypes.ZOMBIE, world);
		mobType = null;
	}

	@Override
	public void killCustom() {
		String mobS = "";
		if (mobType == MobType.ARTIFACT_GUARDIAN) {
			mobS = "Guardian";
		} else {
			mobS = "Warrior";
		}
		for (int i = 1; i <= 2500; ++i) {
			/*if (Herobrine.getPluginCore().getConfigDB().npc.contains("npc." + mobS + ".Drops." + Integer.toString(i))) {
				final int chance = new Random().nextInt(100);
				if (chance <= Herobrine.getPluginCore().getConfigDB().npc.getInt("npc." + mobS + ".Drops." + Integer.toString(i) + ".Chance")) {
					getBukkitEntity().getLocation().getWorld().dropItemNaturally(getBukkitEntity().getLocation(),
					new ItemStack(Material.getMaterial(i), Herobrine.getPluginCore().getConfigDB().npc.getInt("npc." + mobS + ".Drops." + Integer.toString(i) + ".Count")));
				}
			}*/
		}
		setHealth(0.0f);
	}

	@Override
	public MobType getMobType() {
		return mobType;
	}

}