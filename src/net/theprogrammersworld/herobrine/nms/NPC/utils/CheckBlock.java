package net.theprogrammersworld.herobrine.nms.NPC.utils;

import org.bukkit.Location;
import org.bukkit.World;

public class CheckBlock {

	public static boolean checkBlock(int X, int Y, int Z, World w){
		int Dist = 5;
		int solids = 0;
		
		for (int a = 0; a < Dist; a++){
    		
			int X1 = X+a; // Coord +X
			
			for (int i1 = 0; i1 < Dist; i1++){
    			Location LocX = new Location(w, X1, Y, Z+i1);
    			if (!w.getBlockAt(LocX).getType().isSolid()){
    				solids++;
    			}
			}
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocX = new Location(w, X1, Y, Z-i1);
    			if (!w.getBlockAt(LocX).getType().isSolid()){
    				solids++;
    			}
			}
			
			
            int X2 = X-a; // Coord -X
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocX = new Location(w, X2, Y, Z+i1);
				if (!w.getBlockAt(LocX).getType().isSolid()){
    				solids++;
    			}
			}
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocX = new Location(w, X2, Y, Z-i1);
				if (!w.getBlockAt(LocX).getType().isSolid()){
    				solids++;
    			}
			}
			
			
            int Z1 = Z+a; // Coord +Z
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocY = new Location(w, X+i1, Y, Z1);
				if (!w.getBlockAt(LocY).getType().isSolid()){
    				solids++;
    			}
			}
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocY = new Location(w, X-i1, Y, Z1);
    			if (!w.getBlockAt(LocY).getType().isSolid()){
    				solids++;
    			}
			}
			
            int Z2 = Z-a; // Coord -Z
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocY = new Location(w, X+i1, Y, Z2);
				if (!w.getBlockAt(LocY).getType().isSolid()){
    				solids++;
    			}
			}
			
			for (int i1 = 0; i1 < Dist; i1++){
				Location LocY = new Location(w, X-i1, Y, Z2);
				if (!w.getBlockAt(LocY).getType().isSolid()){
    				solids++;
    			}
			}			
    	}
		if (solids > 50){
			return false;
		}
		return true;
	}
}
