package net.theprogrammersworld.herobrine.hooks;

import com.bekvon.bukkit.residence.api.ResidenceApi;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ResidenceHook {

	public boolean Check() {
		return Bukkit.getServer().getPluginManager().getPlugin("Residence") != null;
	}

	public boolean isSecuredArea(final Location loc) {
		return ResidenceApi.getResidenceManager().getByLoc(loc) != null;
	}

}